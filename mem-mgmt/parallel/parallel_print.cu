// Include statements
#include <cuda.h>     // Main cuda header file (found in $LD_LIBRARY_PATH)
                      // Not strictly necessary from a .cu file
#include <stdio.h>

// Print array kernel (parallel)
__global__ void print_parallel(int *array, int length) {
  
  // Get the index of the thread in the array
  int ti = blockDim.x*blockIdx.x + threadIdx.x;
  if (ti < length) {
    printf("  dev: array[%d] = %d\n", ti, array[ti]);
    array[ti] = 2*array[ti];
  }
}

int main(void)
{
  // Set device
  int dev= 0;
  cudaSetDevice(dev);

  // Declare pointers
  int length = 8;
  int *array, *_array;

  // Allocate memory on host and init
  array = (int*) malloc(length * sizeof(int));

  printf("Initialization:\n");
  for (int i = 0; i < length; i++) {
    array[i] = 2*i + 1;
    printf("  host: array[%d] = %d\n", i, array[i]);
  }

  // Allocate memory on device and copy
  cudaMalloc(&_array, length * sizeof(int));
  cudaMemcpy(_array, array, length * sizeof(int), cudaMemcpyHostToDevice);
  // Note that CPU blocks until memory is copied!

  // Print in serial and adjust the numbers
  printf("1 block of %d threads:\n", length);
  print_parallel<<<1,length>>>(_array, length);
  cudaDeviceSynchronize();

  // Try it with a different thread config
  printf("%d blocks with 1 thread:\n", length);
  print_parallel<<<length, 1>>>(_array, length);
  cudaDeviceSynchronize();

  // Copy back and print
  cudaMemcpy(array, _array, length * sizeof(int), cudaMemcpyDeviceToHost);
  printf("Result on the host:\n", length);
  for (int i = 0; i < length; i++) {
    printf("host: array[%d] = %d\n", i, array[i]);
  }

  // Free memory
  free(array);
  cudaFree(_array);

  return EXIT_SUCCESS;
}
