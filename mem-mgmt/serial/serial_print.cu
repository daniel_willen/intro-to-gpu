// Include statements
#include <cuda.h>     // Main cuda header file (found in $LD_LIBRARY_PATH)
                      // Not strictly necessary from a .cu file
#include <stdio.h>

// Print array kernel (serial)
__global__ void print_serial(int *array, int length) {

  // Loop over the array with the only thread
  for (int i = 0; i < length; i++) {
    printf("device: array[%d] = %d\n", i, array[i]);
    array[i] = 2*array[i];
  }
}

int main(void)
{
  // Set device
  int dev= 0;
  cudaSetDevice(dev);

  // Declare pointers
  int length = 8;
  int *array, *_array;

  // Allocate memory on host and init
  array = (int*) malloc(length * sizeof(int));

  for (int i = 0; i < length; i++) {
    array[i] = 2*i + 1;
    printf("host: array[%d] = %d\n", i, array[i]);
  }

  // Allocate memory on device and copy
  cudaMalloc(&_array, length * sizeof(int));
  cudaMemcpy(_array, array, length * sizeof(int), cudaMemcpyHostToDevice);
  // Note that CPU blocks until memory is copied!

  // Print in serial and adjust the numbers
  print_serial<<<1,1>>>(_array, length);
  cudaDeviceSynchronize();

  // Copy back and print
  cudaMemcpy(array, _array, length * sizeof(int), cudaMemcpyDeviceToHost);
  printf("Result on the host:\n", length);
  for (int i = 0; i < length; i++) {
    printf("host: array[%d] = %d\n", i, array[i]);
  }

  // Free memory
  free(array);
  cudaFree(_array);

  return EXIT_SUCCESS;
}
