#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tick

# Inputs
nx = 100
ny = 100
Lx = np.pi
Ly = np.pi
D = 1
times = (Lx**2/D)*np.array([0, 0.03, 0.06, 0.10])

# Domain
x = np.linspace(0, Lx, nx)
y = np.linspace(0, Ly, ny)

xv, yv = np.meshgrid(x, y, sparse=False, indexing='ij')

soln = np.zeros((np.size(times), nx, ny))

for tt, tval in enumerate(times):
  print(tt, tval)
  for i in range(nx):
    for j in range(ny):
      soln[tt,i,j] = np.sin(xv[i,j]) * np.sin(yv[i,j]) * np.exp(-2*D*tval)
    #
  #

  # Plot
  fig = plt.figure(figsize=(3,3))
  ax = fig.add_subplot(111)

  im = ax.imshow(soln[tt,:,:], extent=(0, Lx, 0, Ly), aspect='equal', origin='lower', interpolation=None, vmin=0, vmax=1, cmap='viridis')

  plt.xlabel(r'$x$')
  plt.ylabel(r'$y$')

  ax.set_xticks(np.pi*np.array([0, 0.25, 0.5, 0.75, 1]))
  ax.set_xticklabels([r'$0$', r'$\frac{\pi}{4}$', r'$\frac{\pi}{2}$', r'$\frac{3\pi}{4}$', r'$\pi$'])

  ax.set_yticks(np.pi*np.array([0, 0.25, 0.5, 0.75, 1]))
  ax.set_yticklabels([r'$0$', r'$\frac{\pi}{4}$', r'$\frac{\pi}{2}$', r'$\frac{3\pi}{4}$', r'$\pi$'])

  plt.title(r'$t = %.2f L_x^2/D$' % (tval*D/Lx**2))

  plt.colorbar(im, ax=ax)

  fig_name = 'analytic-soln-t%d.png' % tt
  plt.savefig(fig_name, format='png', bbox_inches='tight')
#


