/* Daniel Willen, 2019
 *
 * Solve the transient heat conduction problem with homogeneous Dirichlet
 *  boundary conditions:
 *
 *    u(x={0,L}) = u(y={0,L}) = 0
 *
 *  and initial condition:
 *
 *    u(x,y,0) = sin(x) * sin(y)
 *
 *  on the domain 0 <= x,y <= L, with L = pi.
 *
 * This program solves the above problem on a single GPU with the Jacobi method.
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <cuda.h>

#include <thrust/device_ptr.h>
#include <thrust/reduce.h>

#define PI 3.14159265358979323846
#define MAX_THREADS_DIM 16        // Note that this depends on the hardware

/* Note on the structure of this file:
 *  - Cuda device constant memory declarations are at the top
 *  - Functions definitions are in the middle. Functions include:
 *  - - parse_cmdline: Read command-line arguments for domain size
 *  - - jacobi_solver: Advance the soln to the next time step using Jacobi
 *  - - check_error:   Calculate the error b/t the numeric and analytic solns
 *  - The `main' function is at the bottom
 *
 *  Note that it is good practice to use header files and break functions out
 *   into separate files. This has not been done here for simplicity.
 */

/*** Variable Declarations ***/
// These are in device constant memory
__constant__ int _nx;       // Grid points in x
__constant__ int _ny;       // Grid points in y
__constant__ double _pref;  // Jacobi pre-factor


/*** Auxiliary Functions ***/

/* Read the command line inputs */
// - argv[0] is the program name
// - argv[1] is the first input (number of points)
int parse_cmdline(int argc, char *argv[]) {
  int nx;
  if (argc == 2) {
    nx = atoi(argv[1]); // Number of grid points
    printf("Grid is %d by %d\n\n", nx, nx);
  } else {
    printf("Input error. Run like: \n\n");
    printf("  $ ./parallel.c n\n\n");
    printf("  where n is the number of grid cells in one dimension\n");
    exit(EXIT_FAILURE);
  }
  return nx;
}

__global__ void jacobi_solver(double *u, double* u_new) {
  // Declare shared memory
  __shared__ double s_u[MAX_THREADS_DIM * MAX_THREADS_DIM];

  // Shared memory index -- position in 2D subgrid
  int si = threadIdx.x;
  int sj = threadIdx.y;
  int sc = si + sj*blockDim.x;
  int sw = sc - 1;
  int se = sc + 1;
  int ss = sc - blockDim.x;
  int sn = sc + blockDim.x;

  // Subdomain indices -- position in 2D global grid
  //  Subtraction accounts for shared memory overlap
  int ti = blockIdx.x * blockDim.x + threadIdx.x - 2*blockIdx.x;
  int tj = blockIdx.y * blockDim.y + threadIdx.y - 2*blockIdx.y;
  int cc = ti + _nx*tj;

  // Load shared memory -- for ALL grid points!
  if (ti < _nx && tj < _ny) {
    s_u[sc] = u[cc];
  }

  // Ensure initial load is done
  __syncthreads();

  // Perform jacobi -- only on inner domain
  if ((ti >= 1 && ti < (_nx - 1)) && (tj >= 1 && tj < (_ny - 1)) &&
      (si > 0 && si < (blockDim.x-1)) && (sj > 0 && sj < (blockDim.y-1))) {
    u_new[cc] = _pref*(s_u[se] +         // East
                       s_u[sw] +         // West
                       s_u[sn] +         // North
                       s_u[ss]) +        // South
            (1. - 4.*_pref)*s_u[sc];     // Center
  }
}

/* Calculate error */
__global__ void check_error(double *u, double *error, double dx, double dy, double D, double time) { 

  // Subdomain indices
  int ti = blockIdx.x * blockDim.x + threadIdx.x;
  int tj = blockIdx.y * blockDim.y + threadIdx.y;

  // Only use threads in the inside of the domain:
  if ((ti >= 1 && ti < (_nx - 1)) && (tj >= 1 && tj < (_ny - 1))) {
    // Note that the error is at time step n, not n+1
    double u_soln = sin(ti*dx) * sin(tj*dy) * exp(-2.*D*time); 
    error[ti + _nx*tj] = fabs( (u[ti + _nx*tj] - u_soln) / u_soln );
  }
}

/*** Main Function ***/
int main(int argc, char *argv[])
{
  /* Variable declaration */
  double Lx = PI;           // Domain length in x-direction
  double Ly = PI;           // Domain length in y-direction
  double D = 1.;            // Diffusion constant

  int nx, ny;               // Grid points (grid cells + 1)
  double dx, dy;            // Grid spacing
  double dt;                // Time step size
  double sim_time;          // Length of sim time, arbitrary for simplicity
  double pref;              // Pre-factor in the Jacobi method

  double *u;                // Field variable of interest (e.g., temperature)
  double error = 0.;        // Mean percent-difference at each grid point
  error = error;            // To prevent compiler warning

  /* Parse command-line for problem size */
  nx = parse_cmdline(argc, argv);
  ny = nx;                  // Assume a square grid

  /* Allocate CPU memory */
  u = (double*) malloc(nx*ny * sizeof(double));

  /* Initialize variables */
  dx = Lx / (nx - 1);       // Cell width in x-direction
  dy = Ly / (ny - 1);       // Cell width in y-direction
  dt = 0.25*dx*dy/D;        // Limited by diffusive stability
  sim_time = 0.5*Lx*Ly/D;   // Arbitrary simulation length
  pref = D*dt/(dx*dx);      // Jacobi pre-factor

  /* Set the initial condition */
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {
      u[i + nx*j] = sin(i*dx) * sin(j*dy);
    }
  }

  /* GPU memory */
  // Declare the pointers
  double *_u, *_u_new;      // Field variables
  double *_error;           // Error at each point

  // Allocate memory
  cudaMalloc(&_u, nx*ny * sizeof(double));
  cudaMalloc(&_u_new, nx*ny * sizeof(double));
  cudaMalloc(&_error, nx*ny * sizeof(double));

  // Copy initial condition to the device
  cudaMemcpy(_u, u, nx*ny * sizeof(double), cudaMemcpyHostToDevice);

  // Set the new soln and error to 0
  cudaMemset(_u_new, 0., nx*ny * sizeof(double));
  cudaMemset(_error, 0., nx*ny * sizeof(double));

  // Need to explicitly copy data to device constant memory
  cudaMemcpyToSymbol(_nx, &nx, sizeof(int));
  cudaMemcpyToSymbol(_ny, &ny, sizeof(int));
  cudaMemcpyToSymbol(_pref, &pref, sizeof(double));

  // Create thrust pointers to device memory for error calculation
  thrust::device_ptr<double> t_error(_error);

  /* Kernel execution configuration */
  // Set the kernel up for the entire domain. In truth, only need two less than
  //  this in each dimension, since we can ignore the boundary nodes as the BCs
  //  are Dirichlet.
  int threads_x = (nx+2) * ((nx+2) < MAX_THREADS_DIM)
                + MAX_THREADS_DIM * ((nx+2) >= MAX_THREADS_DIM);
  int threads_y = (ny+2) * ((ny+2) < MAX_THREADS_DIM)
                + MAX_THREADS_DIM * ((ny+2) >= MAX_THREADS_DIM);

  int blocks_x = (int) ceil((double) nx / (double) (threads_x - 2));
  int blocks_y = (int) ceil((double) ny / (double) (threads_y - 2));

  dim3 dim_blocks(threads_x, threads_y);
  dim3 num_blocks(blocks_x, blocks_y);

  // The error calculation does not benefit from shared memory, so set up normal
  //  threading.
  threads_x = nx * (nx < MAX_THREADS_DIM)
                + MAX_THREADS_DIM * (nx >= MAX_THREADS_DIM);
  threads_y = ny * (ny < MAX_THREADS_DIM)
                + MAX_THREADS_DIM * (ny >= MAX_THREADS_DIM);

  blocks_x = (int) ceil((double) nx / (double) threads_x);
  blocks_y = (int) ceil((double) ny / (double) threads_y);

  dim3 dim_blocks_err(threads_x, threads_y);
  dim3 num_blocks_err(blocks_x, blocks_y);

  /***************************/
  /* Main Time-Stepping Loop */
  /***************************/
  for (double time = 0.; time <= sim_time; time += dt) {
    jacobi_solver<<<num_blocks, dim_blocks>>>(_u, _u_new);

    check_error<<<num_blocks_err, dim_blocks_err>>>(_u, _error, dx, dy, D, time);

    // Use thrust to do a parallel reduction on the error
    error = thrust::reduce(t_error, t_error + nx*ny, 0., thrust::plus<double>());
    printf("Error at t* = %.5lf is %e\n", time*D/(Lx*Lx), error/(nx*ny));

    // Copy new soln to old. This also blocks to ensure computations are finished.
    cudaMemcpy(_u, _u_new, nx*ny * sizeof(double), cudaMemcpyDeviceToDevice);
  }

  // Copy back, if we wanted to write an output file
  cudaMemcpy(u, _u, nx*ny * sizeof(double), cudaMemcpyDeviceToHost);

  // Free
  free(u);
  cudaFree(_u);
  cudaFree(_u_new);
  cudaFree(_error);
  return EXIT_SUCCESS;
}

