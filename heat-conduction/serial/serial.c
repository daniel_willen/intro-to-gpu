/* Daniel Willen, 2019
 *
 * Solve the transient heat conduction problem with homogeneous Dirichlet
 *  boundary conditions:
 *
 *    u(x={0,L}) = u(y={0,L}) = 0
 *
 *  and initial condition:
 *
 *    u(x,y,0) = sin(x) * sin(y)
 *
 *  on the domain 0 <= x,y <= L, with L = pi.
 *
 * This program solves the above problem on a single CPU with the Jacobi method.
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#define PI 3.14159265358979323846

int main(int argc, char *argv[])
{
  /* Variable declaration */
  double Lx = PI;         // Domain length in x-direction
  double Ly = PI;         // Domain length in y-direction
  double D = 1.;          // Diffusion constant

  int nx, ny;             // Grid points (grid cells + 1)
  double dx, dy;          // Grid spacing
  double dt;              // Time step size
  double sim_time;        // Length of sim time, arbitrary for simplicity
  double pref;            // Pre-factor in the Jacobi method

  double *u, *u_new;      // Field variable of interest (e.g., temperature)
  double u_analytic;      // Place to store analytic soln for error checking
  double error = 0.;      // Error -- average percent difference at each node

  /* Read command line inputs */
  // - argv[0] is the program name
  // - argv[1] is the first input (number of points)
  if (argc == 2) {
    nx = atoi(argv[1]); // Number of grid points
    ny = nx;            // Assume a square grid
    printf("Grid is %d by %d\n\n", nx, ny);
    //printf("Grid has %d by %d cells\n", nx-1, ny-1);
  } else {
    printf("Input error. Run like: \n\n");
    printf("  $ ./serial n\n\n");
    printf("  where n is the number of grid cells in one dimension\n");
    exit(EXIT_FAILURE);
  }

  /* Allocate CPU memory */
  u     = (double*) malloc(nx*ny * sizeof(double));
  u_new = (double*) malloc(nx*ny * sizeof(double));

  /* Initialize variables */
  dx = Lx / (nx - 1);       // Cell width in x-direction
  dy = Ly / (ny - 1);       // Cell width in y-direction
  dt = 0.25*dx*dy/D;        // Limited by diffusive stability
  sim_time = 0.5*Lx*Ly/D;   // Arbitrary simulation length
  pref = D*dt/(dx*dx);      // Jacobi pre-factor

  /* Set the initial condition */
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {
      u[i + nx*j] = sin(i*dx) * sin(j*dy);
      u_new[i + nx*j] = 0.;
    }
  }

  /***************************/
  /* Main Time-Stepping Loop */
  /***************************/
  for (double time = 0.; time <= sim_time; time += dt) {
    // Note that we only need to loop in the center of the domain since the
    //  boundary and initial conditions are Dirichlet and do not change. In a
    //  more complicated problem, e.g., with Neumann boundary conditions, we
    //  would have to re-calculate the values at the boundary in order to
    //  satisfy the boundary condition.

    // Loop over the center of the domain:
    for (int j = 1; j < (ny - 1); j++) {
      for (int i = 1; i < (nx - 1); i++) {
        // Jacobi method:
        u_new[i + nx*j] = pref*(u[(i + 1) + nx*j] +           // East
                                u[(i - 1) + nx*j] +           // West
                                u[i + nx*(j + 1)] +           // North
                                u[i + nx*(j - 1)]) +          // South
                                 (1. - 4.*pref)*u[i + nx*j];  // Center

        // Compare with analytic solution and calculate the mean percent error
        //  per grid point. Note that the error is at time step n, not n+1
        u_analytic = sin(i*dx) * sin(j*dy) * exp(-2.*D*time);
        error += fabs( (u[i + nx*j] - u_analytic) / u_analytic);
      }
    }

    // Print and reset the error error
    printf("Error at t* = %.5lf is %e\n", time*D/(Lx*Lx), error/(nx*ny));
    error = 0.;

    // Save new solution over old solution 
    memcpy(u, u_new, nx*ny * sizeof(double));
  }

  // Free memory
  free(u);
  free(u_new);

  return EXIT_SUCCESS;
}
