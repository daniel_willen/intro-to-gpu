/* Daniel Willen, 2019
 *
 * Cuda-aware MPI example with one-sided MPI communication. Run with
 *
 *  $ mpirun -n N ./test
 *
 *  where N is the number of processors
 * 
 */
#include <cuda.h>
#include <stdio.h>
#include <mpi.h>

// This defines the environment variable that MPI sets specifying its rank
#define ENV_LOCAL_RANK "OMPI_COMM_WORLD_LOCAL_RANK"

int main(int argc, char *argv[])
{
  char *local_rank = NULL;        // rank on node
  int rank;                       // global rank 
  int nprocs;                     // number of processes
  int dev_count = 0;              // ndevs on node
  int device;                     // dev number to set

  /*************************/
  /*** Map procs to GPUs ***/
  /*************************/
  // Get local MPI rank of process to set device
  local_rank = getenv(ENV_LOCAL_RANK);
  if (local_rank != NULL) {
    rank = atoi(local_rank);      // store local in global for now
  }

  // Get number of cuda devices
  cudaGetDeviceCount(&dev_count);

  // Set device:
  //  if CUDA_VISIBLE_DEVICES=1,2
  //    cudaSetDevice[0] maps to 1
  //    cudaSetDevice[1] maps to 2
  device = rank % dev_count;
  cudaSetDevice(device);

  /****************/
  /*** Init mpi ***/
  /****************/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  printf("N%d >> Local rank %d with local device %d\n",
    rank, atoi(local_rank), device);

  /***********************/
  /*** Initialize data ***/
  /***********************/
  int length = 1;
  int local = rank;
  int remote = -1;
  int *_local, *_remote;
  cudaMalloc(&_local, length * sizeof(int));
  cudaMalloc(&_remote, length * sizeof(int));

  printf("N%d >> local = %d, remote = %d\n", rank, local, remote);

  // Copy to device
  cudaMemcpy(_local, &local, length * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(_remote, &remote, length * sizeof(int), cudaMemcpyHostToDevice);

  /**********************/
  /*** Initialize MPI ***/
  /**********************/
  // This creates a window into the _remote data that another process is allowed
  //  to modify
  MPI_Win win;
  MPI_Win_create(_remote,               // Memory to use for the window
                 length * sizeof(int),  // Size of the window
                 sizeof(int),           // Base unit of the size
                 MPI_INFO_NULL,         // Optimization (none here)
                 MPI_COMM_WORLD,        // Default MPI Communicator
                 &win);                 // Window pointer

  /*******************/
  /*** Communicate ***/
  /*******************/
  // Set up the target, which in our simple case is the next higher rank (ring
  //  communication)
  int target = (rank + 1) % nprocs; // to next rank
  printf("N%d >> target = %d\n", rank, target);

  // Open up the window for accessing
  MPI_Win_fence(0, win);

  // Put the local data into the remote window
  MPI_Put(_local,     // Local data to put
          length,     // Number of elements in the local data
          MPI_INT,    // Base size of the local data
          target,     // MPI rank on which to modify data
          0,          // Offset into the target window's memory space
          length,     // Number of elements in the remote data
          MPI_INT,    // Remote type
          win);       // Window specifying where on the target the data should go

  // Note that the combination of specifying remote and local types and lengths
  //  would allow us to communicate between processes that are using different
  //  architectures

  // Close the window
  MPI_Win_fence(0, win);

  // Copy the data back
  cudaMemcpy(&local, _local, length * sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(&remote, _remote, length * sizeof(int), cudaMemcpyDeviceToHost);

  // Print
  printf("N%d >> local = %d, remote = %d\n", rank, local, remote);

  // Free and finalize
  cudaFree(_local);
  cudaFree(_remote);
  MPI_Win_free(&win);

  MPI_Finalize();
  return EXIT_SUCCESS; 
}

