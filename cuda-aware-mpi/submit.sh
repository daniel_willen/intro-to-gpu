#!/bin/sh
# submit.sh
#
#SBATCH --partition=gpu
#SBATCH --time=0-:01:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --gres=gpu:2
#SBATCH --cpus-per-task=6
#SBATCH --job-name=cuda-aware
#SBATCH --output=test

mpirun ./test

# For N separate MPI tasks, run with 
# $ sbatch -nN


