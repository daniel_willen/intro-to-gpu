#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

nx = np.array([8, 16, 32, 64, 128, 256, 512, 1024])
etime = np.array([0.343, 0.519, 1.116, 3.335, 11.898, 45.642, 185.468, 1293.678])
error = np.array([1.648e-1, 5.379e-2, 1.476e-2, 3.833e-3, 9.750e-4, 2.458e-4, 6.169e-5, 1.545e-5])

### Error ##############################
fig = plt.figure(figsize=(3,3))
ax = fig.add_subplot(111)

ax.loglog(nx, error, 'ko')

xpts = np.array([1e1, 1e2])
ax.loglog(xpts, 1e1*xpts**-2, 'k--')
#ax.loglog(xpts, 1e0*xpts**-1, 'k-.')

ax.set_xlabel(r'$n_x$')
ax.set_ylabel(r'$\epsilon$')

ax.legend([r'Test Results', r'$\sim n_x^{-2}$'])

plt.savefig('error_vs_size.png', format='png', bbox_inches='tight')

### Time ###############################
fig = plt.figure(figsize=(3,3))
ax = fig.add_subplot(111)

ax.loglog(nx, etime, 'ko')

xpts = np.array([3e1, 5e2])
ax.loglog(xpts, 0.3e-3*xpts**2, 'k--')

ax.set_xlabel(r'$n_x$')
ax.set_ylabel(r'Elapsed Time [s]')

ax.legend([r'Test Results', r'$\sim n_x^2$'])

plt.savefig('etime_vs_size.png', format='png', bbox_inches='tight')

