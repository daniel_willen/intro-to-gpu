\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{graphicx,color}
\usepackage{cancel}
\usepackage{listings}
\usepackage{enumerate}
\usepackage{empheq}

\lstset{
  frame=tb,
  language=C,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,                   % XXX turn on numbers
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  %breaklines=true,
  breakatwhitespace=true,
  tabsize=2
}

\begin{document}

%	TITLE
\title{AC290 Extreme Computing\\Jacobi Solver Homework}
\author{Dan Willen}
\maketitle

Consider the equation
\begin{equation}\label{eq:heat}
  \frac{\partial u}{\partial t} = D \nabla^2 u,
\end{equation}
on the domain \(0 \leq x,y \leq \pi\), with \(u = u(x,y,t)\), \(D\) the diffusive constant, and \(\nabla^2\) the Laplace operator.
The boundary conditions are of the homogeneous Dirichlet type:
\begin{equation}
  u(x = 0, y) = u(x = \pi, y) = u(x, y = 0) = u(x, y = \pi) = 0,
\end{equation}
and the initial condition is
\begin{equation}
  u(x,y, t = 0) = \sin(x) \sin(y).
\end{equation}
The solution to this equation is
\begin{equation}
  u(x,y,t) = \sin(x) \sin(y) \exp{(-2Dt)}
\end{equation}

Using a second-order central difference in space and first order forward difference in time, the discretization of \eqref{eq:heat} is
\begin{equation}
  u_{i,j}^{n+1} = u_{i,j}^n + \frac{D \Delta t}{\Delta x^2} 
    \left[u_{i+1, j}^n +
          u_{i-1, j}^n +
          u_{i, j+1}^n +
          u_{i, j-1}^n -
          4u_{i, j}^n \right],
\end{equation}
where \(u_{i,j}^{n}\) is the value of \(u\) at the \(n^{th}\) time step at grid point \((i,j)\), \(\Delta t = \Delta x^2 / 4D\) is the time step size, and \(\Delta x\) is the grid spacing in the \(x\) and \(y\) directions.

\vspace{5mm}

\begin{enumerate}
  \item Using Cuda, solve the discretized equations up to a time \(t = \pi^2/D\) using the Jacobi method.
    A skeleton code is provided to assist you, with comments in the locations you should make changes.
    \begin{enumerate}[Step I --]
      \item Declare, allocate, and initialize memory for the field variable \texttt{u} on the CPU.
        You should allocate enough memory for the grid, which has size \(nx \times ny\) and initialize \texttt{u} to the initial condition.
        Make sure you free the memory at the end of the program.
      \item Declare and allocate the GPU memory for \texttt{\_u}, \texttt{\_u\_new}\, and \texttt{\_error}.
        Copy the CPU memory to the GPU; the other two arrays have been initialized to zero for you.
        Make sure you free the memory at the end of the program.
      \item Set up the kernel execution configuration for the GPU kernel based on the input domain size and the maximum threads per dimension,
        which is set at the top of the file as a \texttt{\#define}.
        You will need to determine the number of threads per block and the number of blocks in each direction, as well as set up the \texttt{dim3} variables.
      \item Write a GPU kernel that advances to the next timestep using the Jacobi method.
        This should be done in parallel, not in serial.
      \item Write a GPU kernel that calculates the error between the numerical and analytic solutions at each point.
        Be careful to compare solutions at the correct timestep -- \texttt{\_u\_new} is at \(t = (n+1) \Delta t\) and \texttt{\_u} is at \(t = n \Delta t\).
        Using this result, a parallel reduction using the Thrust parallel algorithms library has been provided to calculate the total error in order to find the average percent error at each grid point.
      \item At the end of the loop, copy the data back to the CPU.
    \end{enumerate}
    \textcolor{blue}{The solution to this is located in } % TODO
\end{enumerate}

Your program should take as an input the number of grid cells on one side of the domain.
This has been set up for you such that the program can be run from the command line like: 
\vspace{3mm}\\ \texttt{./jacobi\_solver.cu n} \vspace{3mm}\\
where \(nx = ny = \) \texttt{n} is the size of one side of the square domain.
The program should output the percent difference between your result and the analytic solution, averaged over all of the grid nodes:
\begin{equation}
  \epsilon = \frac{1}{n_x n_y} \sum_{i=1}^{n_x} \sum_{j=1}^{n_y} \frac{u_{i,j}^n - U_{i,j}^n}{U_{i,j}^n},
\end{equation}
where \(U\) is the analytic solution.

If you have time, discuss the following:
\begin{enumerate}
  \item How does the error change as you increase the resolution? Does this behavior make sense? \\
    \textcolor{blue}{The `perfect' response to this question would include a chart with the error on the vertical axis and grid size on the horizontal axis.
      The response would include a sentence or two discussing the rate at which the solution decreases and whether or not this makes sense with the chosen discretization.
      We expect second order grid convergence in space.} \\
      {\centering
      \includegraphics[width=0.6\textwidth]{./plots/error_vs_size.png}}
  \item How does the runtime scale with the resolution? You can get a rough estimate by using the bash command \texttt{time} when executing your program, like:
\vspace{3mm}\\ \texttt{time ./jacobi\_solver.cu n} \vspace{3mm}\\ and using the result for \texttt{real}. \\
    \textcolor{blue}{No particular answer is required here, but a plot showing the scaling is good.
      A comment about what the scaling is and any logical attempt to explain it is better.
      Students are not expected to have the background to be able to answer this fully, but we expect that the amount of work should scale linearly with the total number of points (this may not be strictly true).} \\
      {\centering
      \includegraphics[width=0.6\textwidth]{./plots/etime_vs_size.png}}
  \item How does the runtime change as you change the kernel execution configuration?
    e.g., play around the \texttt{MAX\_THREADS\_DIM} parameter as well as different schemes for setting up the thread blocks. \\
    \textcolor{blue}{Students should be judged on how thoroughly they attempt to answer the question.
      Any attempt to explore the parameter space is good, and any outside research is even better.}
\end{enumerate}

If there is even extra time, use shared memory in the Jacobi kernel.

\end{document}

