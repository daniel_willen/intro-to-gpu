/* Example of "hello world" on a GPU, with kernel configuration
 *
 * Compile:
 *
 *  nvcc hello_world.cu -o hello_world_cuda
 *
 */

// Include statements
#include <cuda.h>     // Main cuda header file (found in $LD_LIBRARY_PATH)
                      // Not strictly necessary from a .cu file, but necessary
                      // from a .c file
#include <stdio.h>


__global__ void hello_world(int dev, int nx, int ny) {

  // This indexing is very similar to strided arrays
  int ti = blockDim.x*blockIdx.x + threadIdx.x;
  int tj = blockDim.y*blockIdx.y + threadIdx.y;

  // If-statement here prevents out-of-bounds memory access: for example, if we
  //  hadn't artificially selected a domain size that was an integer multiple of
  //  the thread dimensions. If not, then the kernel will execute the full block,
  //  but we want to make sure we are within the bounds of the memory
  if (ti < nx && tj < ny) {
    printf("(%d, %d) >> gridDim.x,y = %d, %d >> blockDim.x,y = %d, %d, " 
            "blockIdx.x,y = %d, %d >> threadIdx.x,y = %d, %d\n", 
            ti, tj, gridDim.x, gridDim.y, blockDim.x, blockDim.y,
            blockIdx.x, blockIdx.y, threadIdx.x, threadIdx.y);
  }
}

int main(void)
{
  // Set device
  int dev = 0;
  cudaSetDevice(dev);

  /* Configure threads */
  // Assume we have a 4x6 grid and want to print from each node. Also assume
  //  that each block is constrained to a 2x3 thread config (with no justification)
  int nx = 4;
  int ny = 6;

  int threads_x = 2;
  int threads_y = 3;

  int blocks_x = (int) ceil((double) nx / (double) threads_x);
  int blocks_y = (int) ceil((double) ny / (double) threads_y);
  printf("CPU: Setting blocks_x,y = %d, %d\n", blocks_x, blocks_y);

  dim3 dim_blocks(threads_x, threads_y);
  dim3 num_blocks(blocks_x, blocks_y);

  hello_world<<<num_blocks, dim_blocks>>>(dev, nx, ny);
  cudaDeviceSynchronize();

  // What we should see from this: 24 printf statements, each from a different
  // thread.
  // What happens if we make ny=5, and remove the if (ti....) statement?

  return EXIT_SUCCESS;
}
