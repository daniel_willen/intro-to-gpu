#!/bin/sh

#SBATCH --partition=gpu
#SBATCH --time=0:01:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --job-name=test
#SBATCH --output=test.out

# Can run commands here, or use ‘echo’ to look at CUDA_VISIBLE_DEVICES
hostname
echo $CUDA_VISIBLE_DEVICES
./hello_world_kernel_config
