/* Example of "hello world" on a GPU
 *
 * Compile:
 *
 *  nvcc hello_world.cu -o hello_world_cuda
 *
 * Note:
 *
 *  Thisis just one example of how to structure Cuda files. It is also
 *  possible (and sometimes preferable) to have main .c file that calls Cuda
 *  functions located in a .cu file.
 *
 */

// Include statements
#include <cuda.h>     // Main cuda header file (found in $LD_LIBRARY_PATH)
                      // Not strictly necessary from a .cu file, but necessary
                      // from a .c file
#include <stdio.h>


//  __device__ refers to code that is called by the GPU
__device__ void hello_world_dev() {
  printf("This is a device kernel\n");
}

//  __global__ refers to code that is called by the CPU
__global__ void hello_world(int dev) {
  printf("Hello world from device %d!\n", dev);
  hello_world_dev();
}


int main(void)
{
  // Devices are listed in the CUDA_VISIBLE_DEVICES environment variable.
  // Cuda enumerates devices starting from zero, e.g.:
  //   CUDA_VISIBLE_DEVICES=1,2 -> cudaSetDevice[0] maps to device "1"
  //                            -> cudaSetDevice[1] maps to device "2"
  int dev = 0;        // We only request one device from Slurm
  cudaSetDevice(dev);

  /* CPU Hello World */
  printf("Hello from the CPU!\n");

  /* GPU Hello World */
  // TODO will talk about kernel execution parameters in a bit. Right now, play
  //  with the numbers and see what happens -- need to compile again!
  hello_world<<<1,1>>>(dev);

  // TODO: GPU kernels are asynchronous with the CPU, and program control
  //  returns to the CPU immediately after launching the kernel.
  //  `cudaDeviceSynchronize();' waits for all previous instructions to finish
  //  before returning control to the CPU.
  // Note: This can get mildly complicated with streams and such.
  cudaDeviceSynchronize();

  return EXIT_SUCCESS;
}
